<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\View\AuthController;
use App\Http\Controllers\View\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/// home 
Route::get('/', [HomeController::class, 'index']);
//
Route::get('admin', function () {
    return redirect('/admin/index.html');
});

Route::get('admin/index.html', function () {
    return view('app.index');
});

//
Route::controller(AuthController::class)->group(function(){
    Route::prefix('auth')->group(function () {
        Route::get('/login','login');
        Route::get('/register','register');
    });
});

