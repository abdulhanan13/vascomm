<?php

namespace App\Models\User\Managers;

use App\Helpers\Managers\ManagerBase as Manager;
use App\Models\User\UserModel;

class Validator extends Manager
{
    protected $user;

    public function __construct(UserModel $user)
    {
        $this->user = $user;
    }

    public function __call($name, $arguments)
    {
        return $this->managerCaller($name, $arguments, $this->user, __NAMESPACE__, 'Validators', 'validate');
    }
}
