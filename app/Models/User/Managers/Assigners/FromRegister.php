<?php

namespace App\Models\User\Managers\Assigners;

use Illuminate\Http\Request;
use App\Models\User\UserModel;

class FromRegister
{

    protected $user;

    public function __construct(UserModel $user)
    {
        $this->user = $user;
    }

    public function assign(Request $request)
    {
        $this->user->fill($request->only([
            'name','email','password'
        ]));

        // decode image base64
        if($request->get('image')){
            $image = "vascomm-".time().".jpg";
            $path = public_path() . "/images/" . $image;
            $img = substr($request->get('image'), strpos($request->get('image'), ",")+1);
            $data = base64_decode($img);
           file_put_contents($path, $data);
            $this->user->image = $image;
        }   

        $this->user->password_confirmation = $request->get('password_confirmation');
        $this->user->role = 'customer';

        return $this->user;
    }
}
