<?php

namespace App\Models\User\Managers\Assigners;

use Illuminate\Http\Request;
use App\Models\User\UserModel;

class FromRequest
{

    protected $user;

    public function __construct(UserModel $user)
    {
        $this->user = $user;
    }

    public function assign(Request $request)
    {
        $this->user->fill($request->only([
            'name','email','approved'
        ]));

        return $this->user;
    }
}
