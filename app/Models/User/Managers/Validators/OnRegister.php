<?php

namespace App\Models\User\Managers\Validators;

use App\Models\User\UserModel;

class OnRegister
{
    protected $user;

    public function __construct(UserModel $user)
    {
        $this->user = $user;
    }

    public function validate()
    {

        $attrValidation = $this->validateAttributes();
        if ($attrValidation->fails()) {
            return $attrValidation->errors()->all();
        }

        return true;
    }

    protected function validateAttributes()
    {
        return \Validator::make($this->user->toArray(), [
              'name' => 'required',
              'email' => 'required',
              'password' => 'required|min:6',
              'password_confirmation'=>'required|same:password',
        ]);
    }
}
