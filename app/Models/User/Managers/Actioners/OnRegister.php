<?php

namespace App\Models\User\Managers\Actioners;

use App\Models\User\UserModel;


class OnRegister
{
    protected $user;

    public function __construct(UserModel $user)
    {
        $this->user = $user;
    }

    public function action()
    {
        $this->user->password = \Hash::make($this->user->password);
        // remove password_confirmation field
        unset($this->user->password_confirmation);
        $this->user->save();
    }
}
