<?php

namespace App\Models\User\Managers\Actioners;

use App\Models\User\UserModel;


class OnDelete
{
    protected $user;

    public function __construct(UserModel $user)
    {
        $this->user = $user;
    }

    public function action()
    {
        $this->user->delete();
    }
}
