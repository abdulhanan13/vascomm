<?php

namespace App\Models\User\Managers\Actioners;

use App\Models\User\UserModel;


class OnUpdate
{
    protected $user;

    public function __construct(UserModel $user)
    {
        $this->user = $user;
    }

    public function action()
    {
        $this->user->save();
    }
}
