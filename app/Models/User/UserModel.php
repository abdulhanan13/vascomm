<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model {
  protected $table = 'users';
  protected $guarded = ['created_at', 'updated_at', 'email_verificated_at'];

  public function action()
  {
      return new Managers\Actioner($this);
  }

  public function assign()
  {
      return new Managers\Assigner($this);
  }

  public function validate()
  {
      return new Managers\Validator($this);
  }
}