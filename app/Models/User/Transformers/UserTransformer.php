<?php

namespace App\Models\User\Transformers;

use League\Fractal;
// use Leaguage
use App\Models\User\UserModel;

class UserTransformer extends Fractal\TransformerAbstract
{

    public function transform(UserModel $user)
    {

        $data = [

            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'image' => $user->image,
            'role' => $user->role,
            'approved' => (bool) $user->approved,
        ];

        return $data;
    }
}
