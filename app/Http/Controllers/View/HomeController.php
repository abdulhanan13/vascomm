<?php

namespace App\Http\Controllers\View;
use App\Helpers\Controllers\ApiBaseController as Controller;

use Illuminate\Http\Request;

class HomeController extends Controller {


  protected $user;

  public function __construct()
  {

  }
  
  public function index()
  {
       return view()->make('home');
  }
}