<?php

namespace App\Http\Controllers\View;
use App\Helpers\Controllers\ApiBaseController as Controller;

use Illuminate\Http\Request;

class AuthController extends Controller {


  protected $user;

  public function __construct()
  {

  }
  
  public function login()
  {
       return view()->make('login');
  }

  public function register()
  {
       return view()->make('register');
  }
}