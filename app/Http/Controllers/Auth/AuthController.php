<?php

namespace App\Http\Controllers\Auth;
use App\Helpers\Controllers\ApiBaseController as Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\User\UserModel;
use Validator;
use JWTAuth;

class AuthController extends Controller {


  protected $user;

  public function __construct()
  {
    $this->user = new UserModel();
    

  }
  
  public function login(Request $request)
  {
        $validator = \Validator::make($request->all(), [
          'email' => 'required',
          'password' => 'required|string|min:6',
        ]);
        
        if ($validator->fails()) {
          return response()->json($validator->errors(), 422);
        }

        $query = $this->user->newQuery();

        $user = $query->where('email',$request->get('email'))->first();
        // exit($user);

        if(!$user){
          return response()->json(['errors' => ['User tidak ditemukan silahkan registrasi']], 400);
        }

        if($user->approved == 0){
          return response()->json(['errors' => [ 'User belum di approve oleh admin, silahkan hubungi admin']], 400);
        }

        // set token jwt

        if ($user->role == 'admin') {
            $isAdmin = [
              'is_admin' => true,
              'name' => $user->name
            ];
        }else{
            $isAdmin = [
              'is_admin' => false,
              'name' => $user->name
            ];
        }

        $credentials = $request->only('email', 'password');
        
        if (!$token =JWTAuth::attempt($credentials)) {
            return response()->json(['errors' => ['Unauthorized']], 401);
        }

        $token = auth()->claims($isAdmin)->attempt($credentials);

        return $this->generateToken($token);
  }

  protected function generateToken($token){
      return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
      ]);
  }

  public function register(Request $request){

      $user = $this->user->assign()->fromRegister($request);
      
      $validation = $user->validate()->onRegister();
      if ($validation !== true) {
          return $this->formatErrors($validation);
      }
      
      $user->action()->onRegister();

      return response()->json($user);
  }
}