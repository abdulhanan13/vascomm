<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;

use App\Models\User\UserModel;

use App\Models\User\Transformers\UserTransformer;
use App\Helpers\Controllers\ApiBaseController as Controller;

class UserController extends Controller {


  protected $user;

  public function __construct()
  {
    parent::__construct();
    $this->user = new UserModel();
    $this->transformer = new UserTransformer;
    
  }

  
  public function index(Request $request)
  {
      $query = $this->user->newQuery();
      if ($request->has('paginate')) {
          $users = $query->paginate((int) $request->get('paginate', 15));
          return $this->formatCollection($users, [], $users);
      } else {
          $users = $query->get();
          return $this->formatCollection($users);
      }
  }

  public function show($id)
  {

      $user = $this->user->find($id);

      return $this->formatItem($user);

  }

  public function store(Request $request)
  {

      $user = $this->user->assign()->fromRequest($request);

      $validation = $user->validate()->onCreateOrUpdate();
      if ($validation !== true) {
          return $this->formatErrors($validation);
      }
      $user->action()->onCreateOrUpdate();

      return $this->formatItem($user);
  }

  public function update(Request $request, $id)
  {

      $user = $this->user->find($id);
      $user->assign()->fromRequest($request);

      $validation = $user->validate()->onUpdate();
      if ($validation !== true) {
          return $this->formatErrors($validation);
      }

      $user->action()->onUpdate();

      return $this->formatItem($user);

  }

  public function delete($id)
  {
      $user = $this->user->find($id);

      $user->action()->onDelete();

      return response()->json(['data' => true], 202);
  }

}