<header>
  <nav class="navbar fixed-top  navbar-expand-lg navbar-dark bg-dark shadow-sm">
  <div class="container-fluid"  ng-app="NavbarApp" ng-controller="NavbarController as ctrl">
    <a class="navbar-brand" href="#">Tokoku</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse right-0" id="navbarNavDropdown">
      <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a ng-if="!ctrl.token" class="nav-link text-white" href="{{url('auth/login')}}" style="text-decoration:none;">Login</a>
        </li>
        <li class="nav-item">
        <a ng-if="!ctrl.token" class="nav-link text-white" href="{{url('auth/register')}}" style="text-decoration:none;">Register</a> 
        </li>
        <li  ng-if="ctrl.token"  class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
          <i class="fa fa-user"></i> @{{ctrl.text}}
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <li><button class="dropdown-item" ng-click="ctrl.logout()">Logout</button></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
</header>
<script type="text/javascript">

var app =angular
  .module('NavbarApp',['angular-jwt'])
  .controller('NavbarController',function($http,$scope,$window,$filter,jwtHelper){
    var vm = this;
    vm.token=  localStorage.getItem('access_token');
    vm.text = vm.token ? jwtHelper.decodeToken(vm.token).name : '';
    
    vm.logout = function(){
      if (confirm('Yakin mau logout?')) {
        localStorage.removeItem("access_token");
        window.location.reload()
      }
      
    }
  });
</script>