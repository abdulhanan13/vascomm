admin
    .controller('UserShowController', function(UserModel, $scope, $http, $state) {

        var vm = this
        vm.user = {}
       
        vm.store = function(user) {
            UserModel.update(user.id, user)
            .then((res) => {
                alert('Data Berhasil Disimpan')
                $state.go('userShow', { id: res.data.data.id })
            }) 
        }

        if ($state.params.id) {

            UserModel.get($state.params.id)
                .then(function(res) {

                    vm.user = res.data.data
                    vm.user.image_url = window.location.origin + '/images/' + vm.user.image
                })
        }
    })