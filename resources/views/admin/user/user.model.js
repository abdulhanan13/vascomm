admin
    .factory('UserModel', function(
        $http,
        LinkFactory) {

        var userModel = {}

        userModel.index = function(params) {
            console.log(params)
            return $http.get(LinkFactory.admin.user, { params: params })
        }

        userModel.get = function(id, params) {
            return $http.get(LinkFactory.admin.user+ '/' + id, { params: params })
        }

        userModel.store = function(body, params) {
            return $http.post(LinkFactory.admin.user, body, { params: params })
        }

        userModel.update = function(id, body, params) {
            return $http.put(LinkFactory.admin.user+ '/' + id, body, { params: params })
        }
        userModel.delete = function(id, body, params) {
            return $http.delete(LinkFactory.admin.user+ '/' + id, body, { params: params })
        }

        return userModel
    })