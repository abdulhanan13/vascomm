admin
    .config(function($stateProvider, $urlRouterProvider, $locationProvider) {

        $locationProvider.hashPrefix('');
        $urlRouterProvider.otherwise('/index');

        $stateProvider
            .state('index', {
                url: '/index',
                templateUrl: '/admin/index/index.html',
                controller: 'IndexController as ctrl',
                requireLogin: true,
                pageTitle: 'Admin'
            })
            .state('logout', {
                url: '/logout',
                controller: 'LogoutController as ctrl',
                requireLogin: true,
                pageTitle: 'Admin'
            })
            .state('userIndex', {
                url: '/user/index',
                templateUrl: '/admin/user/index/userIndex.html',
                controller: 'UserIndexController as ctrl',
                requireLogin: true,
                pageTitle: 'Website Admin | User List'
            })
            .state('userShow', {
                url: '/user/show/:id',
                templateUrl: '/admin/user/show/userShow.html',
                controller: 'UserShowController as ctrl',
                requireLogin: true,
                pageTitle: 'Website Admin | User'
            })
            
    })