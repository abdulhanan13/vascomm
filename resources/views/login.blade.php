<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}">
  <script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('assets/jquery/jquery.js')}}"></script>
  <script src="{{asset('assets/angularjs/angular.min.js')}}"></script>
  <script src="{{asset('assets/angularjs/angular-jwt.min.js')}}"></script>
  <title>Login</title>
</head>
<body>
    <div class="container-fluid">
        <div class="row align-items-center vh-100" ng-app="LoginApp" ng-controller="LoginController as ctrl">
            <div class="col-6 mx-auto">
              <div class="card">
                <div class="card-header">
                    <h6 class="card-title">Login</h6>
                </div>
                <div class="card-body">
                  <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Email</label>
                        <input type="email" class="form-control" ng-model="ctrl.auth.email">
                      </div>
                      <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <input type="password" class="form-control" ng-model="ctrl.auth.password">
                      </div>
                      <button type="submit" class="btn btn-primary" ng-click="ctrl.login(ctrl.auth)">Login</button>
                  </div>
              </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

      var app =angular
        .module('LoginApp',['angular-jwt'])
        .controller('LoginController',function($http,$scope,$window,$filter,jwtHelper){
          var vm = this;
          var token='';
          vm.login = function(user){
              $http({
                  method:'POST',
                  data:jQuery.param(user),
                  url : window.location.origin + '/api/auth/login',
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
              })
              .then(function(res){
                  alert('berhasil login')
                  localStorage.setItem('access_token', res.data.access_token)
                  token = jwtHelper.decodeToken(res.data.access_token);
                  if(token.is_admin){
                    window.location.href = window.location.origin + '/admin/index.html'
                  }else{
                    window.location.href = window.location.origin
                  }
              },function(err){

                  alert(err.data.errors[0]);          
              })
          }
        
        });
    </script>
</body>
</html>