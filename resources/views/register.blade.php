<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}">
  <script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('assets/jquery/jquery.js')}}"></script>
  <script src="{{asset('assets/angularjs/angular.min.js')}}"></script>
  <script src="{{asset('assets/webcam/webcam.min.js')}}"></script>

  <title>Login</title>
</head>
<body>
    <div class="container-fluid">
        <div class="row align-items-center vh-100" ng-app="LoginApp" ng-controller="LoginController as ctrl">
            <div class="col-6 mx-auto">
              <div class="card">
                <div class="card-body">
                      <div class="mb-3">
                        <button class="btn btn-info" ng-click="ctrl.openCam= !ctrl.openCam;">Ambil foto selfie</button>
                      </div>
                      <div ng-show="ctrl.openCam">
                        <webcam 
                        channel="ctrl.channel" 
                                on-stream="ctrl.onStream(stream)"
                                on-access-denied="ctrl.onError(err)"
                                on-streaming="ctrl.onSuccess()">
                        </webcam>
                        <button class="btn btn-sm btn-success" ng-click="ctrl.take()">Ambil foto</button>
                      </div>
                      <canvas id="snapshot" width="300" height="300"></canvas>
                      <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Nama</label>
                        <input type="text" class="form-control" ng-model="ctrl.user.name">
                      </div>
                      <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Email</label>
                        <input type="email" class="form-control" ng-model="ctrl.user.email">
                      </div>
                      <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <input type="password" class="form-control" ng-model="ctrl.user.password">
                      </div>
                      <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Konfirmasi Password</label>
                        <input type="password" class="form-control" ng-model="ctrl.user.password_confirmation">
                      </div>
                      <button type="submit" class="btn btn-primary" ng-click="ctrl.register(ctrl.user)">Login</button>
                  </div>
              </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

      var app =angular
        .module('LoginApp',['webcam'])
        .controller('LoginController',function($http,$scope,$window,$filter){
          var vm = this;
          vm.user = {};

          vm.openCam = false;
          vm.channel = {};

          var _video = null,
             patData = null;

          vm.patOpts = {x: 0, y: 0, w: 25, h: 25};
          
          vm.webcamError = false;
          vm.onError = function (err) {
              $scope.$apply(
                  function() {
                      vm.webcamError = err;
                  }
              );
          };

        vm.onSuccess = function () {
        // The video element contains the captured camera data
        _video = vm.channel.video;
              $scope.$apply(function() {
                  vm.patOpts.w = _video.width;
                  vm.patOpts.h = _video.height;
                  //$scope.showDemos = true;
              });
          };

          // take a foto
          vm.take = function() {
            console.log('take')
              if (_video) {
                  var patCanvas = document.querySelector('#snapshot');
                  if (!patCanvas) return;

                  patCanvas.width = _video.width;
                  patCanvas.height = _video.height;
                  var ctxPat = patCanvas.getContext('2d');

                  var idata = getVideoData(vm.patOpts.x, vm.patOpts.y,vm.patOpts.w,vm.patOpts.h);
                  ctxPat.putImageData(idata, 0, 0);

                  sendSnapshotToServer(patCanvas.toDataURL());

                  patData = idata;
              }
          };

          var sendSnapshotToServer = function sendSnapshotToServer(imgBase64) {
            vm.user.image = imgBase64;
          };

          var getVideoData = function getVideoData(x, y, w, h) {
              var hiddenCanvas = document.createElement('canvas');
              hiddenCanvas.width = _video.width;
              hiddenCanvas.height = _video.height;
              var ctx = hiddenCanvas.getContext('2d');
              ctx.drawImage(_video, 0, 0, _video.width, _video.height);
              return ctx.getImageData(x, y, w, h);
          };

          vm.register = function(user){
            // console.log(user)

              $http({
                  method:'POST',
                  data:jQuery.param(user),
                  url : window.location.origin + '/api/auth/register',
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
              })
              .then(function(res){
                  alert('berhasil registrasi, silahkan hubungi admin untuk mengaktifkan akun');
                  window.location.href = window.location.origin + '/auth/login'
              },function(err){
                  alert(err.data.errors[0]);          
              })
          }
        });
    </script>
</body>
</html>