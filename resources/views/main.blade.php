<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Tokoku</title>
  <meta name="description"  content="" >

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
  <!-- Vendor CSS Files -->
  <link href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">

  <style>
      body {
          font-family: 'Nunito', sans-serif;
      }
  </style>

  <!-- <script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script> -->
  <script src="{{asset('assets/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('assets/bootstrap/js/pooper.min.js')}}"></script>
  <script src="{{asset('assets/angularjs/angular.min.js')}}"></script>
  <script src="{{asset('assets/angularjs/angular-jwt.min.js')}}"></script>
</head>

<body>
@include('layouts.navbar')

  <main>
    @yield('content')
  </main>
  
</body>

</html>