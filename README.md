### intalasi
1. composer install
2. copy .env.example .env
3. php artisan key:generate
4. php artisan migrate
5. php artisan db:seed
6. php artisan jwt:secret
7. npm install
8. npm run dev

### admin
email = admin@gmail.com
pass = 12345678

url admin = '{{url}}/admin/index.html'

