var admin = angular
    .module('Admin', [
        'ui.router', 'JwtManager', 'ui.bootstrap','Pagination'
    ])
    .factory('AppFactory', function() {

        var appFactory = {};

        return appFactory;
    });
var admin = angular
    .module('Admin', [
        'ui.router', 'JwtManager', 'ui.bootstrap','Pagination'
    ])
    .factory('AppFactory', function() {

        var appFactory = {};

        return appFactory;
    });
admin
    .controller('IndexController', function() {

        var vm = this

    })
angular
    .module('JwtManager', ['angular-jwt'])
    .factory('JwtValidator', function(
        LinkFactory, jwtHelper) {

        /*
        	This package REQUIRE Link Factory which have LinkFactory.authorization.login and  LinkFactory.authorization.logout links
        	This package REQUIRE URI.js

        */

        var jwtValidator = {};

        var jwtName = 'access_token';

        jwtValidator.decodeToken = function(token) {

            try {

                return jwtHelper.decodeToken(jwtValidator.encodedJwt)

            } catch (e) {

                jwtValidator.unsetJwt()
                return null
            }
        }

        jwtValidator.isLoggedIn = function() {
            if (jwtValidator.encodedJwt == null) {
                return false;
            };

            try {

                if (jwtHelper.isTokenExpired(jwtValidator.encodedJwt)) {
                    alert('Sesi sudah berakhir. Harap login ulang.')
                    JwtValidator.unsetJwt();
                    return false;
                };

            } catch (e) {

                jwtValidator.unsetJwt();
                location.reload();
            }

            return true;
        }

        jwtValidator.setJwt = function(jwt) {

            localStorage.setItem(jwtName, jwt);
        }

        jwtValidator.unsetJwt = function() {

            try {
                localforage.clear();
            } catch (e) {
                console.log(e);
            }

            localStorage.clear();
        }

        jwtValidator.login = function(params) {

            var uri = new URI(window.location.href);
            var hash = uri.hash();

            params.redirect = uri.fragment("");
            params.state = hash;

            window.location.href = new URI(LinkFactory.authentication.login).search(params).toString();

        }

        jwtValidator.encodedJwt = localStorage.getItem(jwtName);

        jwtValidator.decodedJwt = jwtValidator.encodedJwt == null ? null : jwtValidator.decodeToken(jwtValidator.encodedJwt);

        return jwtValidator;
    })
    .run(function($rootScope, $state, $location, JwtValidator,jwtHelper) {

        $rootScope.$on('$stateChangeStart', function(event, toState) {
            if (toState.requireLogin != false) {

                var isLoggedIn = JwtValidator.isLoggedIn();

                if (!isLoggedIn) {
                    alert('Anda perlu login untuk menggunakan fitur ini.');
                    event.preventDefault();
                    window.location.href = '/auth/login';
                };

                var token = jwtHelper.decodeToken(localStorage.getItem('access_token'))
              
                if(!token.is_admin) {
                    window.location.href = window.location.origin;
                }

                
            };



        });


        var retrieveJwt = function() {

            var uri = new URI(window.location.href)

            if (typeof uri.search(true).jwt != 'undefined') {

                JwtValidator.setJwt(uri.search(true).jwt);
                var state = uri.search(true).state;
                uri.search('');


                if (typeof state != "undefined") {

                    window.location.href = uri + state;

                } else {

                    window.location.href = uri;
                };

            };

        }

        retrieveJwt();

    })
    .service('JwtInterceptor', function(JwtValidator) {

        this.request = function(config) {

            if (JwtValidator.encodedJwt != null) {
                config.headers['Authorization'] = 'Bearer ' + JwtValidator.encodedJwt;
            };

            return config;
        }

    })
    .service('JwtErrorInterceptor', function($q, JwtValidator) {

        var interceptors = {};

        interceptors.responseError = function(res) {

            if (res.status == 401 && res.data == 'Session verification failed' && JwtValidator.encodedJwt != null) {
                alert('Maaf, sesi Anda telah habis. Silahkan login ulang.');
                JwtValidator.unsetJwt();
                location.reload();
            };

            return $q.reject(res);
        }

        return interceptors;
    })
    .config(function($httpProvider) {
        $httpProvider.interceptors.push('JwtInterceptor');
        $httpProvider.interceptors.push('JwtErrorInterceptor');
    });
admin
    .factory('LinkFactory', function() {

        var domains = {
            admin: window.location.protocol + '//' + window.location.host + '/',
        }

        var apps = {
            authentication: domains.admin + 'auth/user/',
            admin: domains.admin
        }


        var urls = {

                authentication: {
                    login: domains.admin + 'auth/login'
                },

                admin: {
                    user: apps.admin + 'api/user'
                },
            }

        return urls
    })
admin
    .config(function($stateProvider, $urlRouterProvider, $locationProvider) {

        $locationProvider.hashPrefix('');
        $urlRouterProvider.otherwise('/index');

        $stateProvider
            .state('index', {
                url: '/index',
                templateUrl: '/admin/index/index.html',
                controller: 'IndexController as ctrl',
                requireLogin: true,
                pageTitle: 'Admin'
            })
            .state('logout', {
                url: '/logout',
                controller: 'LogoutController as ctrl',
                requireLogin: true,
                pageTitle: 'Admin'
            })
            .state('userIndex', {
                url: '/user/index',
                templateUrl: '/admin/user/index/userIndex.html',
                controller: 'UserIndexController as ctrl',
                requireLogin: true,
                pageTitle: 'Website Admin | User List'
            })
            .state('userShow', {
                url: '/user/show/:id',
                templateUrl: '/admin/user/show/userShow.html',
                controller: 'UserShowController as ctrl',
                requireLogin: true,
                pageTitle: 'Website Admin | User'
            })
            
    })
admin
    .controller('UserIndexController', function(UserModel) {

        var vm = this

        vm.filter = {
            page:1,
            paginate: 10
        }

        vm.load = function(page) {

            var filter = {
                page:page,
                paginate:10
            }
            UserModel.index(filter)
                .then((res) => {
                    vm.users = res.data.data
                    vm.meta = res.data.meta
                    vm.totalcount = res.data.meta.pagination?.total
                })

        }
        vm.load(1)

        vm.approve = function(user){
            UserModel.update(user.id, {approved: true})
            .then((res) => {
                alert('user berhasil di approve')
                window.location.reload()
            })
        }
        vm.unapprove = function(user){
            UserModel.update(user.id, {approved: false})
            .then((res) => {
                alert('user berhasil di unapprove')
                window.location.reload()

            })
        }
    })
admin
    .controller('LogoutController', function(UserModel) {

        var vm = this

       if(confirm('yakin mau logout?')){
           localStorage.removeItem('access_token');
           window.location.href = window.location.origin
       }
    })
admin
    .controller('UserShowController', function(UserModel, $scope, $http, $state) {

        var vm = this
        vm.user = {}
       
        vm.store = function(user) {
            UserModel.update(user.id, user)
            .then((res) => {
                alert('Data Berhasil Disimpan')
                $state.go('userShow', { id: res.data.data.id })
            }) 
        }

        if ($state.params.id) {

            UserModel.get($state.params.id)
                .then(function(res) {

                    vm.user = res.data.data
                    vm.user.image_url = window.location.origin + '/images/' + vm.user.image
                })
        }
    })
admin
    .factory('UserModel', function(
        $http,
        LinkFactory) {

        var userModel = {}

        userModel.index = function(params) {
            console.log(params)
            return $http.get(LinkFactory.admin.user, { params: params })
        }

        userModel.get = function(id, params) {
            return $http.get(LinkFactory.admin.user+ '/' + id, { params: params })
        }

        userModel.store = function(body, params) {
            return $http.post(LinkFactory.admin.user, body, { params: params })
        }

        userModel.update = function(id, body, params) {
            return $http.put(LinkFactory.admin.user+ '/' + id, body, { params: params })
        }
        userModel.delete = function(id, body, params) {
            return $http.delete(LinkFactory.admin.user+ '/' + id, body, { params: params })
        }

        return userModel
    })