admin
    .controller('UserIndexController', function(UserModel) {

        var vm = this

        vm.filter = {
            page:1,
            paginate: 10
        }

        vm.load = function(page) {

            var filter = {
                page:page,
                paginate:10
            }
            UserModel.index(filter)
                .then((res) => {
                    vm.users = res.data.data
                    vm.meta = res.data.meta
                    vm.totalcount = res.data.meta.pagination?.total
                })

        }
        vm.load(1)

        vm.approve = function(user){
            UserModel.update(user.id, {approved: true})
            .then((res) => {
                alert('user berhasil di approve')
                window.location.reload()
            })
        }
        vm.unapprove = function(user){
            UserModel.update(user.id, {approved: false})
            .then((res) => {
                alert('user berhasil di unapprove')
                window.location.reload()

            })
        }
    })